<?php

namespace App;

use App\Exceptions\SocketException;
use Console\Config;
use Swoole\Client;
use Swoole\Server;

class ControlSocket
{

    private bool $attached = false;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * ControlSocket constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param Server $server
     * @throws SocketException
     */
    public function attachTo(Server $server): void
    {
        if ($this->attached) {
            throw new SocketException('Socket is already attached.');
        }
        $socketsDir = $this->config->get('dir.sockets');
        if (!file_exists($socketsDir)) {
            mkdir($socketsDir, 0777, true);
        }
        $server->addListener($socketsDir . '/control.sock', 0, SWOOLE_SOCK_UNIX_DGRAM);
        $server->on('packet', function (Server $server, string $data, array $info) {
            $matched = false;
            foreach ($server->ports as $port) {
                if (
                    $port->sock == $info['server_socket'] &&
                    $port->port == $info['server_port'] &&
                    $port->type == SWOOLE_SOCK_UNIX_DGRAM
                ) {
                    $matched = true;
                }
            }

            if ($matched) {
                if (trim($data) == 'shutdown') {
                    $server->shutdown();
                }
                if (
                    trim($data) == 'reload' &&
                    isset($server->setting['production']) &&
                    $server->setting['production'] !== true
                ) {
                    $server->reload();
                }
            }
        });
        $server->on('shutdown', function (Server $server) {
            if (isset($server->setting['pid_file']) && file_exists($server->setting['pid_file'])) {
                unlink($server->setting['pid_file']);
            }
        });
        $this->attached = true;
    }

    /**
     * @param string $data
     * @return bool
     */
    public function send(string $data): bool
    {
        $controlSocket = $this->getSocketPath();
        if (file_exists($controlSocket)) {
            $client = new Client(SWOOLE_SOCK_UNIX_DGRAM);
            if ($client->connect($controlSocket, 0)) {
                $client->send($data);
                $client->close();
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    private function getSocketPath(): string
    {
        $socketsDir = $this->config->get('dir.sockets');
        return $socketsDir . '/control.sock';
    }
}
