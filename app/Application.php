<?php

namespace App;

use App\Exceptions\ApplicationException;
use App\Exceptions\InitializationException;
use DI\ContainerBuilder;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7\Stream;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Ilex\SwoolePsr7\SwooleServerRequestConverter as RequestConverter;
use Ilex\SwoolePsr7\SwooleResponseConverter as ResponseConverter;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Application
{

    private string $root;
    private bool $production;

    private App $slim;
    private RequestConverter $requestConverter;
    private Psr17Factory $psr17Factory;

    /**
     * Application constructor.
     * @param string $rootDir
     * @param bool $production
     * @throws ApplicationException
     */
    public function __construct(
        string $rootDir = '/',
        bool $production = false
    ) {
        $this->root = $rootDir;
        $this->production = $production;
        $this->psr17Factory = new Psr17Factory();
        $this->requestConverter = new RequestConverter(
            $this->psr17Factory,
            $this->psr17Factory,
            $this->psr17Factory,
            $this->psr17Factory
        );
        $this->slim = $this->buildSlim($this->psr17Factory);

        // TODO: Implement catch-all error handling, maybe whoops??

        //TODO: Remove this, as this is just for testing.
        $this->getSlim()->get('/', function (RequestInterface $request, ResponseInterface $response) {
            //$response->getBody()->write('Hello World3');
            $stream = Stream::create();
            $stream->write('Hello World 5');
            return $response->withBody($stream);
        });
    }

    /**
     * @return App
     */
    public function getSlim(): App
    {
        return $this->slim;
    }

    /**
     * @param Request $swooleRequest
     * @param Response $swooleResponse
     */
    public function run(Request $swooleRequest, Response $swooleResponse): void
    {
        $psrRequest = $this->requestConverter->createFromSwoole($swooleRequest);
        $psrResponse = $this->getSlim()->handle($psrRequest);
        $responseConverter = new ResponseConverter($swooleResponse);
        $responseConverter->send($psrResponse);
    }

    public function shutdown(): void
    {
        // TODO: Implement logic(if needed) that closes the application properly.
    }

    /**
     * @return ContainerInterface
     * @throws ApplicationException|InitializationException
     */
    private function buildContainer(): ContainerInterface
    {
        try {
            $builder = new ContainerBuilder();
            //$builder->useAutowiring(false);
            //$builder->useAnnotations(false);

            // TODO: implement import of definitions here.

            return $builder->build();
        } catch (\Exception $exception) {
            throw new InitializationException(
                'Failed building application container.',
                $exception->getCode(),
                $exception
            );
        }
    }

    /**
     * @param Psr17Factory $psr17Factory
     * @return App
     * @throws ApplicationException
     */
    private function buildSlim(Psr17Factory $psr17Factory): App
    {
        $app = AppFactory::create($psr17Factory, $this->buildContainer());
        $app->addRoutingMiddleware();
        //TODO: Finish setup of slim here.
        $errorMiddleware = $app->addErrorMiddleware(
            true,
            true,
            true
        );
        return $app;
    }
}
