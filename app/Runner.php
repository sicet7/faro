<?php

declare(strict_types=1);

namespace App;

use App\Exceptions\ApplicationException;
use Console\Config;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;

class Runner
{
    /**
     * @var Server|null
     */
    private ?Server $server;

    /**
     * @var ControlSocket
     */
    private ControlSocket $controlSocket;

    /**
     * @var Application|null
     */
    private ?Application $application = null;

    /**
     * Runner constructor.
     * @param Config $config
     * @param Server|null $server
     */
    public function __construct(Config $config, Server $server = null)
    {
        $this->server = $server;
        $this->controlSocket = new ControlSocket($config);
    }

    /**
     * @return Server|null
     */
    public function getServer(): Server
    {
        return $this->server;
    }

    /**
     * @return ControlSocket
     */
    public function getControlSocket(): ControlSocket
    {
        return $this->controlSocket;
    }

    /**
     * @return int
     */
    public function run(): int
    {
        if ($this->server->start()) {
            return 0;
        }
        return 1;
    }

    /**
     * @throws Exceptions\SocketException
     */
    public function init(): void
    {
        $this->initWorkerStart();
        $this->initRequest();
        $this->initWorkerStop();
        $this->controlSocket->attachTo($this->server);
    }

    private function initWorkerStart(): void
    {
        $this->server->on('workerstart', function (Server $server, int $workerId) {
            if (
                isset($server->setting['chroot']) &&
                file_exists($server->setting['chroot']) &&
                is_dir($server->setting['chroot'])
            ) {
                //TODO: Log this action.
                chdir($server->setting['chroot']);
            }
            try {
                $this->application = new Application(
                    getcwd(),
                    isset($server->setting['production']) && $server->setting['production'] === true
                );
            } catch (ApplicationException $exception) {
                // TODO: make proper logging.
                // TODO: make kill switch to prevent infinite loop on continues ApplicationException's
                echo $exception->__toString() . PHP_EOL;
                echo 'Exception occurred in worker start for worker: #' . $workerId . PHP_EOL;
                echo 'Exiting Worker: #' . $workerId . PHP_EOL;
                $server->stop($workerId);
            }
        });
    }

    private function initRequest(): void
    {
        $this->server->on('request', function (Request $request, Response $response) {
            $this->application->run($request, $response);
        });
    }

    private function initWorkerStop(): void
    {
        $this->server->on('workerstop', function (Server $server, int $workerId) {
            $this->application->shutdown();
        });
    }
}
