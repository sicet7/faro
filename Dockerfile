FROM php:7.4-cli-alpine

ARG SWOOLE_TAG="v4.5.3"

RUN apk add --update --no-cache postgresql-dev && \
    apk add --update --no-cache --virtual buildDeps \
            tzdata \
            git \
            openssl \
            curl \
            curl-dev \
            procps \
            libxml2-dev \
            oniguruma-dev \
            autoconf \
            gcc \
            make \
            g++ \
            zlib-dev && \
            docker-php-ext-install \
            pdo_mysql \
            mysqli \
            mbstring \
            json \
            opcache \
            sockets \
            pcntl \
            pgsql \
            xml \
            simplexml \
            curl \
            pdo \
            posix \
            bcmath && \
            pecl install redis && docker-php-ext-enable redis && \
            cd /usr/local/lib && \
            git clone https://github.com/swoole/swoole-src.git swoole && \
            cd swoole && \
            git checkout ${SWOOLE_TAG} && \
            phpize && \
            ./configure --enable-openssl --enable-sockets --enable-http2 --enable-mysqlnd && \
            make && make install && \
            docker-php-ext-enable swoole && \
            echo "swoole.fast_serialize=On" >> /usr/local/etc/php/conf.d/docker-php-ext-swoole-serialize.ini && \
            cd ~ && \
            curl -sS https://getcomposer.org/installer | php && \
            mv composer.phar /usr/local/bin/composer && \
            composer self-update --clean-backups && \
            cp /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
            echo 'Etc/UTC' > /etc/timezone && \
            echo "[Date]\ndate.timezone=Etc/UTC" > /usr/local/etc/php/conf.d/timezone.ini && \
            apk del buildDeps

WORKDIR /srv/swoole

COPY . .

RUN rm -rf vendor && \
    rm -rf var && \
    composer install --no-dev && \
    mkdir -p var/log && \
    mkdir -p var/sockets && \
    mkdir -p var/upload && \
    chown -R www-data:www-data .

USER www-data:www-data

EXPOSE 9501

ENTRYPOINT ["php", "run", "server:start", "--no-daemon", "--port=9501"]