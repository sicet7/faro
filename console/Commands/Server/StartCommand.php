<?php

declare(strict_types=1);

namespace Console\Commands\Server;

use App\Exceptions\SocketException;
use App\Runner;
use Console\Config;
use Console\Exceptions\ServerBuilderException;
use Console\Server\Builder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StartCommand extends Command
{
    public const COMMAND_NAME = 'server:start';
    protected const OPTION_HOST = 'host';
    protected const OPTION_PORT = 'port';
    protected const OPTION_WORKERS = 'workers';
    protected const OPTION_MAX_REQUESTS = 'max-requests';
    protected const OPTION_DISPATCH_MODE = 'dispatch-mode';
    protected const OPTION_REUSE_PORT = 'reuse-port';
    protected const OPTION_TCP_NODELAY = 'tcp-nodelay';
    protected const OPTION_PRODUCTION = 'production';
    protected const OPTION_NO_DAEMON = 'no-daemon';

    /**
     * @var Builder
     */
    private Builder $builder;

    /**
     * @var Config
     */
    private Config $config;


    /**
     * StartCommand constructor.
     * @param Builder $builder
     * @param Config $config
     * @inheritDoc
     */
    public function __construct(Builder $builder, Config $config, string $name = null)
    {
        parent::__construct($name);
        $this->builder = $builder;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(static::COMMAND_NAME);
        $this->setProcessTitle(static::COMMAND_NAME);
        $this->addOption(
            self::OPTION_HOST,
            null,
            InputOption::VALUE_OPTIONAL,
            'The host the server should be listening on.',
            '0.0.0.0'
        );
        $this->addOption(
            self::OPTION_PORT,
            null,
            InputOption::VALUE_OPTIONAL,
            'The port the server should be listening on.',
            9501
        );
        $this->addOption(
            self::OPTION_WORKERS,
            null,
            InputOption::VALUE_OPTIONAL,
            'The number of worker threads.',
            5
        );
        $this->addOption(
            self::OPTION_MAX_REQUESTS,
            null,
            InputOption::VALUE_OPTIONAL,
            'The number of requests a worker can handle in its lifecycle.',
            2000
        );
        $this->addOption(
            self::OPTION_DISPATCH_MODE,
            null,
            InputOption::VALUE_OPTIONAL,
            'See: https://www.swoole.co.uk/docs/modules/swoole-server/configuration#dispatch_mode',
            1
        );
        $this->addOption(
            self::OPTION_REUSE_PORT,
            null,
            InputOption::VALUE_OPTIONAL,
            'Enable reuse ports.',
            false
        );
        $this->addOption(
            self::OPTION_TCP_NODELAY,
            null,
            InputOption::VALUE_OPTIONAL,
            'Disable Nagle\'s algorithm',
            false
        );
        $this->addOption(
            self::OPTION_PRODUCTION,
            'prod',
            InputOption::VALUE_OPTIONAL,
            'Run the application in production mode',
            false
        );
        $this->addOption(
            self::OPTION_NO_DAEMON,
            null,
            InputOption::VALUE_OPTIONAL,
            'Run the application in the current shell instead of a daemon',
            false
        );
    }

    /**
     * @inheritDoc
     */
    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $this->booleanFlag($input, self::OPTION_REUSE_PORT);
        $this->booleanFlag($input, self::OPTION_TCP_NODELAY);
        $this->booleanFlag($input, self::OPTION_PRODUCTION);
        $this->booleanFlag($input, self::OPTION_NO_DAEMON);
        $this->numericOption($input, self::OPTION_PORT);
        $this->numericOption($input, self::OPTION_MAX_REQUESTS);
        $this->numericOption($input, self::OPTION_DISPATCH_MODE);
        $this->numericOption($input, self::OPTION_WORKERS);
    }

    /**
     * @inheritDoc
     * @throws RuntimeException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $dispatchMode = $input->getOption(self::OPTION_DISPATCH_MODE);
        $workers = $input->getOption(self::OPTION_WORKERS);
        $host = $input->getOption(self::OPTION_HOST);
        $port = $input->getOption(self::OPTION_PORT);
        $maxRequest = $input->getOption(self::OPTION_MAX_REQUESTS);
        $reusePort = $input->getOption(self::OPTION_REUSE_PORT);
        $tcpNoDelay = $input->getOption(self::OPTION_TCP_NODELAY);
        $noDaemon = $input->getOption(self::OPTION_NO_DAEMON);
        $production = $input->getOption(self::OPTION_PRODUCTION);
        try {
            $this->builder->setDispatchMode((int) $dispatchMode);
            $this->builder->setWorkerNumber((int) $workers);
            $this->builder->setHost($host);
            $this->builder->setPort((int) $port);
            $this->builder->setMaxWorkerRequest((int) $maxRequest);
            $this->builder->setPortReuse($reusePort);
            $this->builder->setTcpNodelay($tcpNoDelay);
            $this->builder->setDaemonize(!$noDaemon);
            $this->builder->setProductionMode($production);
            $server = $this->builder->build();
            $applicationRunner = new Runner($this->config, $server);
            $applicationRunner->init();
            return $applicationRunner->run();
        } catch (ServerBuilderException $serverBuilderException) {
            throw new RuntimeException(
                'Server Build Error.',
                $serverBuilderException->getCode(),
                $serverBuilderException
            );
        } catch (SocketException $socketException) {
            throw new RuntimeException(
                'Socket Exception.',
                $socketException->getCode(),
                $socketException
            );
        }
    }

    /**
     * @param InputInterface $input
     * @param string $key
     * @throws RuntimeException
     */
    private function numericOption(InputInterface $input, string $key): void
    {
        if ($input->hasOption($key)) {
            if (is_numeric($input->getOption($key))) {
                $input->setOption($key, (int) $input->getOption($key));
            } else {
                throw new RuntimeException('Invalid value in: "' . $key . ' must be a valid number".');
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param string $key
     */
    private function booleanFlag(InputInterface $input, string $key): void
    {
        if ($input->hasOption($key) && $input->getOption($key) !== false) {
            $input->setOption($key, true);
        }
    }
}
