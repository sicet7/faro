<?php

namespace Console\Commands\Server;

use App\Runner;
use Console\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StopCommand extends Command
{
    /**
     * @var Config
     */
    private Config $config;

    public function __construct(Config $config, string $name = null)
    {
        parent::__construct($name);
        $this->config = $config;
    }

    protected function configure()
    {
        $this->setName('server:stop');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $runner = new Runner($this->config);
        if ($runner->getControlSocket()->send('shutdown')) {
            $output->writeln('Successfully pushed "shutdown" packet onto ControlSocket.');
            return 0;
        }
        return 1;
    }
}
