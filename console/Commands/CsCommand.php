<?php

declare(strict_types=1);

namespace Console\Commands;

use Console\Application;
use Console\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CsCommand extends Command
{
    private Config $config;

    public function __construct(Config $config, string $name = null)
    {
        $this->config = $config;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('cs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = $this->config->get('dir.root');
        if (empty($root)) {
            throw new RuntimeException('Invalid Root directory, config key "dir.root" returned invalid value');
        }
        $commandExecutable = $root . '/vendor/bin/phpcs';
        if (!file_exists($commandExecutable)) {
            throw new RuntimeException('Failed to find command executable: "' . $commandExecutable . '".');
        }
        passthru('php ' . $commandExecutable . ' --basepath=' . escapeshellarg($root) . '', $return);
        return $return;
    }
}
