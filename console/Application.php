<?php

declare(strict_types=1);

namespace Console;

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application as SymfonyApplication;
use Symfony\Component\Console\CommandLoader\CommandLoaderInterface;
use Symfony\Component\Finder\Finder;

class Application
{
    //TODO: Move the definitions directory up one dir and make it shared between console and web application.
    public const DEFINITIONS_DIRECTORY = __DIR__ . '/definitions';

    /**
     * @var SymfonyApplication
     */
    private SymfonyApplication $app;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * @var string
     */
    private string $appRoot;

    /**
     * Application constructor.
     * @param string $name The name of the console application
     * @throws \Exception
     */
    public function __construct(string $name)
    {
        $finder = new Finder();

        $containerBuilder = (new ContainerBuilder())
            ->useAnnotations(false)
            ->useAutowiring(false);

        $containerBuilder->addDefinitions([
            Application::class => $this,
        ]);
        foreach ($finder->files()->in(self::DEFINITIONS_DIRECTORY) as $file) {
            $containerBuilder->addDefinitions($file->getPathname());
        }

        $this->container = $containerBuilder->build();
        $this->app = new SymfonyApplication($name, $this->getVersion());
    }

    /**
     * @return ContainerInterface The Console Applications Container.
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return SymfonyApplication The Symfony Console Application that runs the commands.
     */
    public function getSymfonyApplication(): SymfonyApplication
    {
        return $this->app;
    }

    /**
     * @return int The return status of the application.
     * @throws \Exception
     */
    public function run(): int
    {
        $this->init();
        return $this->getSymfonyApplication()->run();
    }

    /**
     * @return string The root directory of the application
     */
    protected function getRoot(): string
    {
        if (!isset($this->appRoot)) {
            $this->appRoot = (string) $this->getContainer()->get(Config::class)->get('dir.root');
        }
        return $this->appRoot;
    }

    /**
     * @return string The version gotten from the composer.json or 'UNKNOWN'
     */
    private function getVersion(): string
    {
        $file = $this->getRoot() . '/composer.json';
        if (file_exists($file)) {
            $json = file_get_contents($file);
            $data = json_decode($json, true);
            if (json_last_error() === JSON_ERROR_NONE && array_key_exists('version', $data)) {
                return $data['version'];
            }
        }
        return 'UNKNOWN';
    }

    private function init(): void
    {
        $this->getSymfonyApplication()->setCommandLoader(
            $this->getContainer()->get(CommandLoaderInterface::class)
        );
    }
}
