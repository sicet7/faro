<?php

namespace Console;

use Console\Exceptions\ConfigException;

class Config
{
    private const DELIMITER = '.';

    /**
     * @var array[]
     */
    private array $data = [];

    /**
     * @param string $iniFile
     * @return array
     * @throws ConfigException
     */
    public function loadIniFile(string $iniFile): array
    {
        if (!file_exists($iniFile)) {
            throw new ConfigException('Config file not found "' . $iniFile . '".');
        }
        $data = parse_ini_file($iniFile, true, INI_SCANNER_TYPED);
        if ($data === false) {
            throw new ConfigException('Failed to load config from file "' . $iniFile . '".');
        }
        return $data;
    }

    /**
     * @param string $namespace
     * @param array $data
     */
    public function apply(string $namespace, array $data): void
    {
        if (isset($this->data[$namespace])) {
            $this->data[$namespace] = array_replace_recursive($this->data[$namespace], $data);
        } else {
            $this->data[$namespace] = $data;
        }
    }

    /**
     * @param string $key
     * @return mixed|array|null
     */
    public function get(string $key)
    {
        if (strstr($key, self::DELIMITER) !== false) {
            return $this->retrieve($key, $this->data);
        }
        return $this->data[$key];
    }

    private function retrieve(string $key, array $data)
    {
        $key = str_replace(
            self::DELIMITER . self::DELIMITER,
            self::DELIMITER,
            $key
        );
        $keys = explode(self::DELIMITER, trim($key, self::DELIMITER));
        $key = array_shift($keys);
        if (isset($data[$key])) {
            if (count($keys) === 0) {
                return $data[$key];
            }
            if (is_array($data[$key])) {
                return $this->retrieve(implode(self::DELIMITER, $keys), $data[$key]);
            }
        }
        return null;
    }
}
