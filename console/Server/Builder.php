<?php

namespace Console\Server;

use Console\Exceptions\ServerBuilderException;
use Swoole\Http\Server as SHS;

/**
 * Class Builder
 * @package Console\Server\Config
 * @todo Make this builder support WebSocket in the same server.
 */
class Builder
{

    public const DISPATCH_MODE_ROUND_ROBIN = 1;
    public const DISPATCH_MODE_FIXED = 2;
    public const DISPATCH_MODE_PREEMPTIVE = 3;
    public const DISPATCH_MODE_IP_BASED = 4;
    public const DISPATCH_MODE_USER_ID = 5;
    public const DISPATCH_MODE_STREAM = 7;

    public const IPC_MODE_UNIX_SOCKET = 1;
    public const IPC_MODE_MESSAGE_QUEUE = 2;
    public const IPC_MODE_MESSAGE_QUEUE_COMP = 3;

    public const LOG_LEVEL_DEBUG = 0;
    public const LOG_LEVEL_TRACE = 1;
    public const LOG_LEVEL_INFO = 2;
    public const LOG_LEVEL_NOTICE = 3;
    public const LOG_LEVEL_WARNING = 4;
    public const LOG_LEVEL_ERROR = 5;

    private const DISPATCH_MODE_VALUES = [
        self::DISPATCH_MODE_ROUND_ROBIN,
        self::DISPATCH_MODE_FIXED,
        self::DISPATCH_MODE_PREEMPTIVE,
        self::DISPATCH_MODE_IP_BASED,
        self::DISPATCH_MODE_USER_ID,
        self::DISPATCH_MODE_STREAM,
    ];

    private const IPC_MODE_VALUES = [
        self::IPC_MODE_UNIX_SOCKET,
        self::IPC_MODE_MESSAGE_QUEUE,
        self::IPC_MODE_MESSAGE_QUEUE_COMP
    ];

    private const SSL_METHOD_VALUES = [
        SWOOLE_SSLv3_METHOD,
        SWOOLE_SSLv3_SERVER_METHOD,
        SWOOLE_SSLv3_CLIENT_METHOD,
        SWOOLE_SSLv23_METHOD,
        SWOOLE_SSLv23_SERVER_METHOD,
        SWOOLE_SSLv23_CLIENT_METHOD,
        SWOOLE_TLSv1_METHOD,
        SWOOLE_TLSv1_SERVER_METHOD,
        SWOOLE_TLSv1_CLIENT_METHOD,
        SWOOLE_TLSv1_1_METHOD,
        SWOOLE_TLSv1_1_SERVER_METHOD,
        SWOOLE_TLSv1_1_CLIENT_METHOD,
        SWOOLE_TLSv1_2_METHOD,
        SWOOLE_TLSv1_2_SERVER_METHOD,
        SWOOLE_TLSv1_2_CLIENT_METHOD,
    ];

    private const LOG_LEVEL_VALUES = [
        self::LOG_LEVEL_DEBUG,
        self::LOG_LEVEL_TRACE,
        self::LOG_LEVEL_INFO,
        self::LOG_LEVEL_NOTICE,
        self::LOG_LEVEL_WARNING,
        self::LOG_LEVEL_ERROR,
    ];

    private array $config = [
        'open_http_protocol' => true,
        'open_mqtt_protocol' => false,
        'open_websocket_protocol' => false,
    ];

    private int $port = 9501;
    private string $host = '0.0.0.0';
    private int $mode = SWOOLE_PROCESS;
    private int $socketType = SWOOLE_SOCK_TCP;

    /**
     * @param string $file
     * @return $this
     * @throws ServerBuilderException
     */
    public function setPidFile(string $file): self
    {
        if (isset($this->config['pid_file'])) {
            throw new ServerBuilderException('Cannot change "pid_file" when it is already set.');
        }
        if (!file_exists(dirname($file)) || !is_dir(dirname($file))) {
            throw new ServerBuilderException(
                'Cannot set "pid_file" as the parent directory for the file doesn\'t exist.'
            );
        }
        $this->config['pid_file'] = $file;
        return $this;
    }

    /**
     * @param int $mode
     * @param bool $unsafeEvent this is only needed for round robin or preemptive.
     * @return $this
     * @throws ServerBuilderException
     */
    public function setDispatchMode(int $mode, bool $unsafeEvent = false): self
    {
        if (!in_array($mode, self::DISPATCH_MODE_VALUES)) {
            throw new ServerBuilderException(
                'Invalid dispatch_mode value, valid values are: ' . implode(',', self::DISPATCH_MODE_VALUES)
            );
        }
        if (
            isset($this->config['open_http2_protocol']) &&
            $this->config['open_http2_protocol'] === true &&
            $mode != self::DISPATCH_MODE_FIXED &&
            $mode != self::DISPATCH_MODE_IP_BASED
        ) {
            throw new ServerBuilderException(
                'HTTP2 is only supported on dispatch modes: ' .
                self::DISPATCH_MODE_FIXED . ' and ' . self::DISPATCH_MODE_IP_BASED
            );
        }
        $this->config['dispatch_mode'] = $mode;
        if ($mode == self::DISPATCH_MODE_ROUND_ROBIN || $mode == self::DISPATCH_MODE_PREEMPTIVE) {
            $this->config['enable_unsafe_event'] = $unsafeEvent;
        }
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setProductionMode(bool $enabled): self
    {
        $this->config['production'] = $enabled;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     * @throws ServerBuilderException
     */
    public function setHttp2(bool $enable): self
    {
        if (
            $enable &&
            isset($this->config['dispatch_mode']) &&
            $this->config['dispatch_mode'] != self::DISPATCH_MODE_FIXED &&
            $this->config['dispatch_mode'] != self::DISPATCH_MODE_IP_BASED
        ) {
            throw new ServerBuilderException(
                'HTTP2 is only supported on dispatch modes: ' .
                self::DISPATCH_MODE_FIXED . ' and ' . self::DISPATCH_MODE_IP_BASED
            );
        }
        $this->config['open_http2_protocol'] = $enable;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setDaemonize(bool $enable): self
    {
        $this->config['daemonize'] = ($enable ? 1 : 0);
        return $this;
    }

    /**
     * @param int $workers
     * @return $this
     * @throws ServerBuilderException
     */
    public function setWorkerNumber(int $workers): self
    {
        if (isset($this->config['reactor_num']) && $this->config['reactor_num'] >= $workers) {
            throw new ServerBuilderException(
                'Could not set "worker_num", "reactor_num" has to be smaller than "worker_num".'
            );
        }
        $this->config['worker_num'] = $workers;
        return $this;
    }

    /**
     * @param int $reactorNumber
     * @return $this
     * @throws ServerBuilderException
     */
    public function setReactorNumber(int $reactorNumber): self
    {
        if (isset($this->config['worker_num']) && $reactorNumber >= $this->config['worker_num']) {
            throw new ServerBuilderException(
                'Could not set "reactor_num", "reactor_num" has to be smaller than "worker_num".'
            );
        }
        $this->config['reactor_num'] = $reactorNumber;
        return $this;
    }

    /**
     * @param string $serverCrtFile
     * @param string $serverKeyFile
     * @param bool $verifyPeer
     * @param bool $allowSelfSigned
     * @param string|null $clientCrtFile
     * @param int $sslMethod
     * @param string $ciphers
     * @return $this
     * @throws ServerBuilderException
     */
    public function configureSsl(
        string $serverCrtFile,
        string $serverKeyFile,
        bool $verifyPeer = false,
        bool $allowSelfSigned = false,
        string $clientCrtFile = null,
        int $sslMethod = SWOOLE_SSLv23_METHOD,
        string $ciphers = 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH'
    ): self {
        if (!file_exists($serverCrtFile)) {
            throw new ServerBuilderException('Failed to find server CRT file: "' . $serverCrtFile . '".');
        }
        if (!file_exists($serverKeyFile)) {
            throw new ServerBuilderException('Failed to find server KEY file: "' . $serverKeyFile . '".');
        }
        if (!in_array($sslMethod, self::SSL_METHOD_VALUES)) {
            throw new ServerBuilderException(
                'Invlaid SSLMethod, valid values are: ' . implode(',', self::SSL_METHOD_VALUES)
            );
        }
        if (empty($ciphers)) {
            throw new ServerBuilderException('Ciphers cannot be empty.');
        }
        if ($verifyPeer && (empty($clientCrtFile) || !file_exists($clientCrtFile))) {
            throw new ServerBuilderException('Failed to find client CRT file: "' . $clientCrtFile . '".');
        }
        if (($this->getSocketType() & SWOOLE_SSL) !== SWOOLE_SSL) {
            $this->setSocketType($this->getSocketType() | SWOOLE_SSL);
        }
        $this->config['ssl_cert_file'] = $serverCrtFile;
        $this->config['ssl_key_file'] = $serverKeyFile;
        $this->config['ssl_allow_self_signed'] = $allowSelfSigned;
        $this->config['ssl_method'] = $sslMethod;
        $this->config['ssl_ciphers'] = $ciphers;
        if ($verifyPeer) {
            $this->config['ssl_verify_peer'] = $verifyPeer;
            $this->config['ssl_client_cert_file'] = $clientCrtFile;
        }
        return $this;
    }

    /**
     * @param string $user
     * @return $this
     */
    public function setUser(string $user): self
    {
        $this->config['user'] = $user;
        return $this;
    }

    /**
     * @param string $group
     * @return $this
     */
    public function setGroup(string $group): self
    {
        $this->config['group'] = $group;
        return $this;
    }

    /**
     * @param string $root
     * @return $this
     * @throws ServerBuilderException
     */
    public function setChroot(string $root): self
    {
        if (!file_exists($root) || !is_dir($root)) {
            throw new ServerBuilderException(
                'Could not set "chroot" to "' . $root . '" as it dosent exist or isn\'t a directory'
            );
        }
        $this->config['chroot'] = $root;
        return $this;
    }

    /**
     * @param int $max
     * @return $this
     */
    public function setMaxWorkerRequest(int $max): self
    {
        $this->config['max_request'] = $max;
        $this->config['max_request_grace'] = round(($max / 2));
        return $this;
    }

    /**
     * @param string $logFile
     * @param string $dateFormat
     * @param int $logLevel DEBUG = 0, TRACE = 1, INFO = 2, NOTICE = 3, WARNING = 4, ERROR = 5
     * @param string|null $slowLogFile null is allowed if $slowLog is false.
     * @param int $slowLogTimeout time in seconds that a request has to exceed to be logged.
     * @return $this
     * @throws ServerBuilderException
     */
    public function configureLogging(
        string $logFile,
        string $dateFormat = 'Y-m-d\TH:i:sO',
        int $logLevel = 2,
        string $slowLogFile = null,
        int $slowLogTimeout = 2
    ): self {

        if (!file_exists(dirname($logFile))) {
            throw new ServerBuilderException(
                'Cannot set log file, because the log file parent directory doesn\'t exist.'
            );
        }

        if (!in_array($logLevel, self::LOG_LEVEL_VALUES)) {
            throw new ServerBuilderException(
                'Unknown log level, "' . $logLevel . '" is not a known log level.'
            );
        }

        if (!empty($slowLogFile) && !file_exists(dirname($slowLogFile))) {
            throw new ServerBuilderException(
                'Invalid slow log file, make sure the specified path exists.'
            );
        }

        $this->config['log_file'] = $logFile;
        $this->config['log_date_format'] = $dateFormat;
        $this->config['log_level'] = $logLevel;
        if (!empty($slowLogFile)) {
            $this->config['request_slowlog_file'] = $slowLogFile;
            $this->config['request_slowlog_timeout'] = $slowLogTimeout;
        } else {
            $this->config['request_slowlog_file'] = false;
        }
        return $this;
    }

    /**
     * @param bool $enabled
     * @param int $level
     * @return $this
     * @throws ServerBuilderException
     */
    public function setHttpCompression(bool $enabled, int $level = 2): self
    {
        if ($enabled && $level < 1 || $level > 9) {
            throw new ServerBuilderException(
                'Invalid compression level, level must be between 1 and 9, inclusively.'
            );
        }
        $this->config['http_compression'] = $enabled;
        $this->config['http_compression_level'] = $level;
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setHttpParse(bool $enabled): self
    {
        $this->config['http_parse_post'] = $enabled;
        $this->config['http_parse_cookie'] = $enabled;
        return $this;
    }

    /**
     * @param string $dir
     * @return $this
     * @throws ServerBuilderException
     */
    public function setUploadTmpDir(string $dir): self
    {
        if (!file_exists($dir) || !is_dir($dir)) {
            throw new ServerBuilderException(
                'Invalid temp upload directory. "' . $dir . '" doesn\'t exist.'
            );
        }
        $this->config['upload_tmp_dir'] = $dir;
        return $this;
    }

    /**
     * @param bool $enabled
     * @param string|null $documentRoot
     * @return $this
     * @throws ServerBuilderException
     */
    public function setStaticFileHandler(bool $enabled, string $documentRoot = null): self
    {
        if ($enabled && (empty($documentRoot) || !file_exists($documentRoot) || !is_dir($documentRoot))) {
            throw new ServerBuilderException(
                'Document root must be defined to a valid directory when enabling the static file handler.'
            );
        }
        $this->config['enable_static_handler'] = $enabled;
        if ($enabled) {
            $this->config['document_root'] = $documentRoot;
        } elseif (isset($this->config['document_root'])) {
            unset($this->config['document_root']);
        }
        return $this;
    }

    /**
     * @param array $locations
     * @return $this
     * @throws ServerBuilderException
     */
    public function setStaticLocations(array $locations): self
    {
        foreach ($locations as $location) {
            if (!file_exists($location) || !is_dir($location)) {
                throw new ServerBuilderException(
                    'Static location: "' . $location . '" is not a known directory'
                );
            }
        }
        if (!isset($this->config['enable_static_handler']) || $this->config['enable_static_handler'] !== true) {
            throw new ServerBuilderException(
                'Cannot set static locations when the static file handler is disabled'
            );
        }
        $this->config['static_handler_locations'] = $locations;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setCoroutine(bool $enable): self
    {
        $this->config['enable_coroutine'] = $enable;
        return $this;
    }

    /**
     * @param int $mb
     * @return $this
     */
    public function setOutputBufferSize(int $mb): self
    {
        $this->config['buffer_output_size'] = ($mb * 1024 * 1024);
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setPortReuse(bool $enable): self
    {
        $this->config['enable_reuse_port'] = $enable;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setTcpNodelay(bool $enable): self
    {
        $this->config['open_tcp_nodelay'] = $enable;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setReloadAsync(bool $enable): self
    {
        $this->config['reload_async'] = $enable;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     */
    public function setTcpFastopen(bool $enable): self
    {
        $this->config['tcp_fastopen'] = $enable;
        return $this;
    }

    /**
     * @param int $waitTime
     * @return $this
     */
    public function setMaxWaitTime(int $waitTime): self
    {
        $this->config['max_wait_time'] = $waitTime;
        return $this;
    }

    /**
     * @param bool $enable
     * @return $this
     * @throws ServerBuilderException
     */
    public function setDiscardTimeoutRequest(bool $enable): self
    {
        if (
            !isset($this->config['dispatch_mode']) ||
            ($this->config['dispatch_mode'] != self::DISPATCH_MODE_ROUND_ROBIN &&
                $this->config['dispatch_mode'] != self::DISPATCH_MODE_PREEMPTIVE)
        ) {
            throw new ServerBuilderException(
                '"discard_timeout_request" is only relevant if the dispatch_mode is set to ' .
                self::DISPATCH_MODE_ROUND_ROBIN . ' or ' . self::DISPATCH_MODE_PREEMPTIVE
            );
        }
        $this->config['discard_timeout_request'] = $enable;
        return $this;
    }

    /**
     * @param int $maxConn
     * @return $this
     */
    public function setMaxConnections(int $maxConn): self
    {
        $this->config['max_conn'] = $maxConn;
        return $this;
    }

    #region Tasks

    /**
     * @param int $ipcMode
     * @return $this
     * @throws ServerBuilderException
     */
    public function setTaskIpcMode(int $ipcMode): self
    {
        if (!in_array($ipcMode, self::IPC_MODE_VALUES)) {
            throw new ServerBuilderException(
                'Invalid IPC mode, valid values are: ' . implode(',', self::IPC_MODE_VALUES)
            );
        }
        $this->config['task_ipc_mode'] = $ipcMode;
        return $this;
    }

    /**
     * @param string $key
     * @return $this
     * @throws ServerBuilderException
     */
    public function setMessageQueueKey(string $key): self
    {
        if (
            isset($this->config['task_ipc_mode']) &&
            !in_array($this->config['task_ipc_mode'], [self::IPC_MODE_MESSAGE_QUEUE, self::IPC_MODE_MESSAGE_QUEUE_COMP])
        ) {
            throw new ServerBuilderException(
                'Cannot set: "message_queue_key" as the IPC Mode is not set to use a message queue.'
            );
        }
        $this->config['message_queue_key'] = $key;
        return $this;
    }

    /**
     * @param int $twn
     * @return $this
     */
    public function setTaskWorkerNumber(int $twn): self
    {
        $this->config['task_worker_num'] = $twn;
        return $this;
    }

    /**
     * @param string $tmpDir
     * @return $this
     * @throws ServerBuilderException
     */
    public function setTaskTmpDir(string $tmpDir): self
    {
        if (!file_exists($tmpDir) || !is_dir($tmpDir)) {
            throw new ServerBuilderException(
                'Could not set Task Temp Directory as the provided directory doesn\'t exist.'
            );
        }
        $this->config['task_tmpdir'] = $tmpDir;
        return $this;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setTaskMaxRequest(int $number): self
    {
        $this->config['task_max_request'] = $number;
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setTaskCoroutine(bool $enabled): self
    {
        $this->config['task_enable_coroutine'] = $enabled;
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    public function setTaskUseObject(bool $enabled): self
    {
        $this->config['task_use_object'] = $enabled;
        return $this;
    }

    #endregion

    #region Parameters

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @param int $port
     * @return $this
     */
    public function setPort(int $port): self
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost(string $host): self
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     * @return $this
     */
    public function setMode(int $mode): self
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return int
     */
    public function getSocketType(): int
    {
        return $this->socketType;
    }

    /**
     * @param int $socketType
     * @return $this
     */
    public function setSocketType(int $socketType): self
    {
        $this->socketType = $socketType;
        return $this;
    }

    #endregion

    /**
     * @return SHS
     */
    public function build(): SHS
    {
        $server = new SHS(
            $this->getHost(),
            $this->getPort(),
            $this->getMode(),
            $this->getSocketType()
        );
        $server->set($this->config);
        return $server;
    }

    /**
     * @return string|null
     */
    public function getPidFile(): ?string
    {
        if (isset($this->config['pid_file'])) {
            return $this->config['pid_file'];
        }
        return null;
    }
}
