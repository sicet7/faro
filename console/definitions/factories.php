<?php

use Console\Factories\CommandFactory;
use DI\Invoker\FactoryParameterResolver;
use Psr\Container\ContainerInterface;

use function DI\create;
use function DI\get;

return [
    FactoryParameterResolver::class =>
        create(FactoryParameterResolver::class)
            ->constructor(get(ContainerInterface::class)),

    CommandFactory::class =>
        create(CommandFactory::class)
            ->constructor(get(FactoryParameterResolver::class)),
];
