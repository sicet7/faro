<?php

use Console\Config;
use Console\Server\Builder;

return [
    Builder::class => function (Config $cfg) {
        $builder = new Builder();

        $allowedConfig = [
            'log_level',
            'log_date_format',
            'request_slowlog_timeout',
            'buffer_output_size',
            'enable_static_handler',
            'http_compression',
            'http_compression_level',
            'open_http2_protocol',
            'user',
            'group',
            'ssl_allow_self_signed',
            'ssl_verify_peer',
            'ssl_ciphers',
            'ssl_method',
        ];
        $paths = [
            'log_file',
            'request_slowlog_file',
            'upload_tmp_dir',
            'document_root',
            'ssl_cert_file',
            'ssl_key_file',
            'ssl_client_cert_file',
        ];

        $root = $cfg->get('dir.root');
        $config = $cfg->get('server');
        foreach ($config as $key => $value) {
            if (!in_array($key, $allowedConfig) && !in_array($key, $paths)) {
                unset($config[$key]);
                break;
            }
            if (in_array($key, $paths) && is_string($value)) {
                if (empty($value)) {
                    $config[$key] = $root;
                } else {
                    $config[$key] = $root . '/' . ltrim($value, './');
                }
            }
        }

        $builder->setPidFile($root . '/.server.pid');
        $builder->setChroot($root);

        if (isset($config['open_http2_protocol'])) {
            $builder->setHttp2($config['open_http2_protocol']);
        }
        if (isset($config['user'])) {
            $builder->setUser($config['user']);
        }
        if (isset($config['group'])) {
            $builder->setGroup($config['group']);
        }
        if (isset($config['http_compression']) && isset($config['http_compression_level'])) {
            $builder->setHttpCompression($config['http_compression'], $config['http_compression_level']);
        }
        if (isset($config['upload_tmp_dir'])) {
            $builder->setUploadTmpDir($config['upload_tmp_dir']);
        }
        if (isset($config['enable_static_handler']) && isset($config['document_root'])) {
            $builder->setStaticFileHandler($config['enable_static_handler'], $config['document_root']);
        }
        if (isset($config['buffer_output_size'])) {
            $builder->setOutputBufferSize($config['buffer_output_size']);
        }
        if (isset($config['log_file'])) {
            $logFile = $config['log_file'];
            $logLevel = 2;
            $logDateFormat = 'Y-m-d\TH:i:sO';
            $requestSlowlogFile = null;
            $requestSlowlogTimeout = 2;
            if (isset($config['log_level'])) {
                $logLevel = (int) $config['log_level'];
            }
            if (isset($config['log_date_format'])) {
                $logDateFormat = $config['log_date_format'];
            }
            if (isset($config['request_slowlog_file'])) {
                $requestSlowlogFile = $config['request_slowlog_file'];
            }
            if (isset($config['request_slowlog_timeout'])) {
                $requestSlowlogTimeout = $config['request_slowlog_timeout'];
            }
            $builder->configureLogging(
                $logFile,
                $logDateFormat,
                $logLevel,
                $requestSlowlogFile,
                $requestSlowlogTimeout
            );
        }
        if (isset($config['ssl_cert_file']) && isset($config['ssl_key_file'])) {
            $sslCertFile = $config['ssl_cert_file'];
            $sslKeyFile = $config['ssl_key_file'];
            $sslVerifyPeer = false;
            $sslAllowSelfSigned = false;
            $sslClientCertFile = null;
            $sslMethod = SWOOLE_SSLv23_METHOD;
            $sslCiphers = 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
            if (isset($config['ssl_verify_peer']) && isset($config['ssl_client_cert_file'])) {
                $sslVerifyPeer = $config['ssl_verify_peer'];
                $sslClientCertFile = $config['ssl_client_cert_file'];
            }
            if (isset($config['ssl_allow_self_signed'])) {
                $sslAllowSelfSigned = $config['ssl_allow_self_signed'];
            }
            if (
                isset($config['ssl_method']) &&
                is_string($config['ssl_method']) &&
                substr($config['ssl_method'], 0, 7) == 'SWOOLE_' &&
                defined($config['ssl_method'])
            ) {
                $sslMethod = constant($config['ssl_method']);
            }
            if (isset($config['ssl_ciphers'])) {
                $sslCiphers = $config['ssl_ciphers'];
            }
            $builder->configureSsl(
                $sslCertFile,
                $sslKeyFile,
                $sslVerifyPeer,
                $sslAllowSelfSigned,
                $sslClientCertFile,
                $sslMethod,
                $sslCiphers
            );
        }
        return $builder;
    },
];
