<?php

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\CommandLoader\CommandLoaderInterface;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;
use Console\Factories\CommandFactory;
use Console\Commands\CsCommand;
use Console\Commands\Server\StartCommand;
use Console\Commands\Server\StopCommand;
use Console\Commands\Server\ReloadCommand;

use function DI\create;
use function DI\get;
use function DI\factory;

return [
    CommandLoaderInterface::class => create(ContainerCommandLoader::class)
        ->constructor(get(ContainerInterface::class), [
            'server:start' => StartCommand::class,
            'server:stop' => StopCommand::class,
            'server:reload' => ReloadCommand::class,
            'cs' => CsCommand::class,
        ]),
    StartCommand::class => factory([CommandFactory::class, 'create']),
    StopCommand::class => factory([CommandFactory::class, 'create']),
    ReloadCommand::class => factory([CommandFactory::class, 'create']),
    CsCommand::class => factory([CommandFactory::class, 'create']),
];
