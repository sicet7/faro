<?php

use Console\Config;

return [
    Config::class => function () {
        $root = dirname(__DIR__, 2);
        $config = new Config();
        $config->apply('dir', [
            'root' => $root,
            'var' => $root . '/var',
            'sockets' => $root . '/var/sockets',
        ]);
        $server = $config->loadIniFile($root . '/config/server.ini');
        $config->apply('server', $server);
        return $config;
    },
];
